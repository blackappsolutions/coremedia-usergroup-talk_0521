### Goody Bag - Setup GitLab Build-Runner at digitalocean.com
<small>https://cloud.digitalocean.com</small>
#### Initial setup of digitalocean cli 'doctl'
<small>https://github.com/digitalocean/doctl</small>
```shell
# mac only
brew install doctl
# Create a token first at https://cloud.digitalocean.com/account/api/tokens
doctl auth init
doctl compute size list
# Slug                 Memory    VCPUs    Disk    Price Monthly    Price Hourly
# s-4vcpu-8gb          8192      4        160     40.00            0.059520
doctl compute region list
# Slug    Name               Available
# fra1    Frankfurt 1        true
```
#### Create on demand Build-Runner
```shell
# https://docs.digitalocean.com/reference/doctl/reference/compute/droplet/create/
doctl compute droplet create \
--image ubuntu-20-04-x64 \
--size s-4vcpu-8gb \
--region fra1 \
--ssh-keys 'THE_FINGERPRINT_OF_YOUR_SSH_KEY' \
--user-data-file YOUR_INIT_SCRIPT \
YOUR_BUILD_RUNNER.HOST.TLD
# Determine IP Address
doctl compute droplet list
```
<small>You can also create images of your droplet => https://cloud.digitalocean.com/images</small>
#### Create `.ssh/config` with IP 
```
host digiocean
    HostName THE_IP_FROM_ABOVE_GOES_HERE
    User root
    IdentityFile YOUR_PRIVATE_KEY
```

#### Check droplet initialization                                                                                                                                                  
```shell
ssh digiocean
less /var/log/cloud-init-output.log
Cloud-init v. 20.3-2-g371b392c-0ubuntu1~20.04.1 finished at Mon, 26 Apr 2021 06:45:47 .. Up 146.90 seconds
```
<small>Check, if runner is alive at GitLab => https://gitlab.com/YOUR_USER/YOUR_PROJECT/-/settings/ci_cd#js-runners-settings</small>

#### Memory usage during build
```shell
root@cmcc-build-runner:~# free -h
              total        used        free      shared  buff/cache   available
Mem:          7.8Gi       1.8Gi       121Mi       1.0Mi       5.8Gi       5.6Gi
```
<small>
=> So we don't need the 8GB RAM for the build,<br> 
BUT the 4vcpu's (which were only avail. for 8GB droplets)<br> 
to build the whole blueprint from scratch in 33min!!!

=> Partial build (only headless) => 14min
</small>
