## GitLab CI/CD
* Build/Deploy-Pipelines are defined in in your repository as YAML: `.gitlab-ci.yml`
* <!-- .element: class="fragment" data-fragment-index="1" -->Your jobs can run on GitLabs
  [shared runners](https://docs.gitlab.com/ee/ci/runners/#shared-runners) (Google Cloud Platform)
  * <!-- .element: class="fragment" data-fragment-index="2" -->400 min/month => $0 (FREE)
  * <!-- .element: class="fragment" data-fragment-index="3" -->10.000 min/month => $19 (Premium)
  * <!-- .element: class="fragment" data-fragment-index="4" -->50.000 min/month => $99 (Ultimate)
  <small class="fragment" data-fragment-index="4">https://about.gitlab.com/pricing/gitlab-com/feature-comparison/</small>  
* <!-- .element: class="fragment" data-fragment-index="5" -->Or on your own
  [specific runner](https://docs.gitlab.com/ee/ci/runners/#create-a-specific-runner) - my recommendation for CoreMedia

