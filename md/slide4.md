## Set up your own specific Build-Runner
... with docker executor on ubuntu
```sh []
# Install docker
apt-get -y update
apt-get -y install docker.io
# Install gitlab-runner
curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64
chmod +x /usr/local/bin/gitlab-runner
useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash
/usr/local/bin/gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
service gitlab-runner start
# Register with project token https://gitlab.com/blackappsolutions/coremedia-blueprints/-/settings/ci_cd#coordinator_address
gitlab-runner register --non-interactive --url "https://gitlab.com/" --registration-token "ENTER_YOUR_YOUR_TOKEN_HERE" \
--executor "docker" --description "cm_blueprints_build_runner" --tag-list "cmcc_2010" --run-untagged="true" \
--docker-image provocon/coremedia-build:latest --docker-privileged="true"
# Final adjustments
sed -i 's|"/cache"|"/certs/client", "/cache"|' /etc/gitlab-runner/config.toml && gitlab-runner restart
```


