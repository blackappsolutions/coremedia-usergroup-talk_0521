## Build CoreMedia Blueprints
Docker in Docker Build Setup
```yaml
image: provocon/coremedia-build:latest
stages:
  - common-stage
variables: &dockerInDockerBuildVars
  GIT_SUBMODULES: init
  GIT_SUBMODULE_STRATEGY: recursive
  CONT_REGISTRY: $CI_REGISTRY
  CONT_REGISTRY_USER: $CI_REGISTRY_USER
  CONT_REGISTRY_PASSWORD: $CI_REGISTRY_PASSWORD
  DOCKER_STRATEGY: exec         
  DOCKER_DRIVER: overlay2       
  DOCKER_TLS_CERTDIR: "/certs"  

# Generate & store deployment archives as docker containers
.build-apps: &build-apps
  stage: common-stage
  cache:
    key: "maven_repository"
    paths:
      - mvn_repo
      - .remote-packages
  services:
    - docker:19.03.1-dind 
  before_script:
    - mkdir ~/.m2 && cp $CI_PROJECT_DIR/workspace-configuration/maven-settings.xml ~/.m2/settings.xml
  script:
    - if ! test -z "$apps"; then onlySelectiveApps="-am -pl $apps"; fi
    - |
      mvn install -Pwith-docker -DskipTests -Dmdep.analyze.skip=true -Denforcer.skip=true -B --no-transfer-progress \
      -Dmaven.repo.local=mvn_repo -DskipThemes=true -DskipContent=true -Dskip-joo-unit-tests=true $onlySelectiveApps
    - .gitlab-ci/tagAndPushImages.sh $apps # not covered in this demo

Build-All:
  variables:
    <<: *dockerInDockerBuildVars
  <<: *build-apps
  when: manual

Build-Headless:
  variables:
    <<: *dockerInDockerBuildVars
    apps: ":headless-server"
  <<: *build-apps
  only:
    changes:
      - apps/headless-server/*
...
```
<small class="fragment" data-fragment-index="4">https://gitlab.com/provocon/coremedia-build-docker/-/blob/master/example-usage/dot.gitlab-ci-example.yml</small>
